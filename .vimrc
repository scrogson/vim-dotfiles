set encoding=utf-8
set nocompatible " be iMproved
filetype off            " required for Vundle

set rtp+=~/.vim/bundle/vundle
call vundle#rc()

" let Vundle manage Vundle
Bundle 'gmarik/vundle'

" Vundle help
""""""""""""""
" :BundleList            - list configured bundles
" :BundleInstall(!)      - install (update) bundles
" :BundleSearch(!) foo   - search (or refresh cache first) for foo
" :BundleClean(!)        - confirm (or auto-approve) removal of unused bundles

" System
Bundle 'tpope/vim-fugitive'
Bundle 'msanders/snipmate.vim'
Bundle 'krisleech/snipmate-snippets'
Bundle 'scrooloose/nerdcommenter'
Bundle 'surround.vim'
Bundle 'vim-scripts/AutoClose'
Bundle 'tpope/vim-git'
Bundle 'mileszs/ack.vim'

" Ruby specific
Bundle 'vim-ruby/vim-ruby'
Bundle 'tpope/vim-rails'
Bundle 'tpope/vim-rvm'
Bundle 'tpope/vim-bundler'
Bundle 'tpope/vim-rake'
Bundle 'tpope/vim-cucumber'

" Other
Bundle 'tpope/vim-haml'
Bundle 'tpope/vim-markdown'
Bundle 'tpope/vim-pastie'
Bundle 'kchmck/vim-coffee-script'

" Non-github repos
Bundle 'git://git.wincent.com/command-t.git'

" Colorscheme
Bundle 'scrogson/vim-lithium-dark'

filetype plugin indent on " required!

" Colors
colorscheme lithium_dark

hi DiffAdd term=reverse cterm=bold ctermbg=darkgreen ctermfg=black
hi DiffChange term=reverse cterm=bold ctermbg=gray ctermfg=black
hi DiffText term=reverse cterm=bold ctermbg=blue ctermfg=black
hi DiffDelete term=reverse cterm=bold ctermbg=darkred ctermfg=black

"hi CursorLine   cterm=NONE ctermbg=white ctermfg=white guibg=black guifg=white
"hi CursorColumn cterm=NONE ctermbg=black ctermfg=white guibg=black guifg=white

" Basic
let mapleader = ","
let gmapleader = ","
set ignorecase
set smartcase
set gdefault
set incsearch
set cursorline
set scrolloff=3

set autoindent
set smartindent

set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set wildignore=*.swp,tmp,.git,*.png,*.jpg,*.gif

set ttyfast
set ruler
set laststatus=2
set term=screen
set modelines=0
set backspace=indent,eol,start
set history=100
set visualbell " no bell please
set noerrorbells " shut up
set nowrap
set number
set ruler
set colorcolumn=80
set showcmd
set cmdheight=2 " Set the command height to 2 lines
set showmatch " Highlight closing ), >, }, ], etc...
set undolevels=1000
set directory=~/.vim/tmp
set nowrap
set textwidth=79
set formatoptions=qrn1

" Display a place holder character for tabs and trailing spaces
set listchars=tab:▸\ ,eol:¬

" Shortcut to rapidly toggle `set list`
nmap <leader>l :set list!<CR>

nmap <leader>r :set relativenumber<cr>

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
nnoremap ; :
inoremap jj <ESC>
nnoremap / /\v
vnoremap / /\v
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %
" strip all trailing whitespace in the current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>
" fold tag
nnoremap <leader>ft Vatzf
" open .vimrc file
nnoremap <leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>

au FocusLost * :wa
autocmd FileType php setlocal ts=4 sts=4 sw=4 noexpandtab
autocmd FileType javascript,html,css setlocal ts=4 sts=4 sw=4 expandtab
autocmd FileType ruby,pml,erb,haml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType markdown setlocal wrap linebreak nolist
autocmd BufNewFile,BufRead *.rss setfiletype xml
autocmd BufNewFile,BufRead *.scss setfiletype css.scss
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline

if has("autocmd")
  " Enable filetype detection
  filetype plugin indent on
  " Restore cursor position
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
endif

if &t_Co > 2 || has("gui_running")
  " Enable syntax highlighting
  syntax on
endif

let g:surround_{char2nr('-')} = "<% \r %>"
let g:surround_{char2nr('=')} = "<%= \r %>"
let g:surround_{char2nr('8')} = "/* \r */"
let g:surround_{char2nr('s')} = " \r"
let g:surround_{char2nr('^')} = "/^\r$/"
let g:surround_indent = 1
