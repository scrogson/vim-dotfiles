# Vim dotfiles

These are the vim runtime files that I use on a daily basis. Feel free to clone, modify, change, add, murder or remove portions of it at will.

### Requirements
 - You must compile VIM with Ruby support in order for the [Command-T](https://github.com/wincent/Command-T) plugin to work. [jperras](https://github.com/jperras) has a written some great [instructions](http://nerderati.com/2010/07/compiling-vim-with-ruby-integration-on-snow-leopard/) on his blog.

## Usage

**NOTE**: If you have existing files in `$HOME/.vim` or `$HOME/.vimrc`, make sure you take appropriate backups before performing any of the following steps.

 - First, clone this respository into your `$HOME` directory:

```sh
$ git clone git://github.com:scrogson/vim-dotfiles.git ~/.vim && cd ~/.vim
```

 - Next, run the following `rake` tasks:
   - `rake vim:setup` This will symlink the .vimrc file to `$HOME/.vimrc` and create a tmp directory for VIM .swp files
   - `rake vundle:install` will install [Vundle](https://github.com/gmarik/vundle)
   - `rake` Will install of of the vim plugins specified in the .vimrc file and
     will build Command-T and install the snipmate-snippets.

And that's it! Be sure to source your newly installed configuration (`:source $MYVIMRC` from inside Vim, or just restart a new editor session), and you should be good to go.
